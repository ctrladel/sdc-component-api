<?php

namespace Drupal\sdc\Exception;

/**
 * Raised when the template for a component cannot be found.
 */
class TemplateNotFoundException extends \Exception {

}
