<?php

namespace Drupal\sdc\Exception;

/**
 * Raised when the component syntax in Twig is invalid.
 */
class ComponentSyntaxException extends \Exception {

}
