<?php

namespace Drupal\component_api\Exception;

/**
 * Raised when a component cannot be found.
 */
class ComponentNotFoundException extends \Exception {

}
