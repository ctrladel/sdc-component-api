<?php

namespace Drupal\component_api\Exception;

/**
 * Custom exception for invalid components.
 */
class InvalidComponentException extends \Exception {

}
