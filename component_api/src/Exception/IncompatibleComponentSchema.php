<?php

namespace Drupal\component_api\Exception;

/**
 * Custom exception for incompatible schemas.
 */
class IncompatibleComponentSchema extends \Exception {

}
